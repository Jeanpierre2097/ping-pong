
// variables globales y el canvas
var canvas = document.getElementById('canvas'),
	ctx = canvas.getContext('2d'), keydown = [],
	twoPlayers = true, ball_speed_i = 0, winner = 0,
	play = false, score_1 = score_2 = 0 ,background = new Image;


//CREACION DE OBJETOS DEL JUEGO, JUGADORES, BOLA , SONIDOS , FONDOS
var player1 = {x:20,y:250,width:20,height:100},
player2 = {x:760,y:250,width:20,height:100},
ball = {x:100,y:parseInt(Math.random()*570+15),radio:15,dir:1,angle:120};
//SONIDOS
var snd_go = document.createElement('audio');
snd_colision = document.createElement('audio');
game_over= document.createElement('audio');
snd_go.src = 'aud/Flash-laser-04.wav';
snd_colision.src = 'aud/laser1.wav';
game_over.src = 'aud/gameover.wav';
// IMAGEN JUGADORES 
var img_blok = new Image;
img_blok.src = 'img/Block.jpg';


// 	// input del teclado
(function() {window.addEventListener('keydown',function(e) {
	keydown[e.keyCode] = true;
},false);
window.addEventListener('keyup',function(e) {
	keydown[e.keyCode] = false;
},false)
})();


//funcion de backgrounds
function randomBackgroung() {
    var backs = new Array('img/0030.jpg','img/0009.jpg','img/0033.jpg','img/0001.png','img/0020.jpg');
	var i = parseInt(Math.random()*backs.length);
	background.src = backs[i];}
function drawBackground() {
ctx.drawImage(background,0,0,800,600);
}


//DIBUJAMOS EL MARCADOR ASI COMO LOS OBJETOS A UTILIZAR 
function drawPlayers() {
	ctx.save();
	ctx.fillStyle = 'purple';
    if(winner != 0) ctx.globalAlpha = 0;
	ctx.drawImage(img_blok,player1.x,player1.y,player1.width,player1.height);
	ctx.drawImage(img_blok,player2.x,player2.y,player2.width,player2.height);
	ctx.beginPath();
	ctx.arc(ball.x,ball.y,ball.radio,0,7);
	ctx.fill();
	ctx.restore();
	ctx.save();
	ctx.shadowColor = 'red';
	ctx.font = '36px Berlin Sans FB';
	ctx.fillStyle = 'white';
	ctx.textAlign = 'center';
	ctx.verticalAlign = 'middle';
	ctx.fillText('WINNER 10 POINTS!!!',420,27);
	ctx.fillText('Score|\n'+score_1,250,57);
	ctx.fillText('Score |\n'+score_2,550,57);
	ctx.restore();
}

// pantalla principal inicial del juego
function getPlay() {
	ctx.save();
	ctx.shadowOffsetX = shadowOffsetY = 0;
	ctx.shadowBlur = 10;
	ctx.shadowColor = '#fff';
	ctx.fillStyle = 'white';
	ctx.textAlign = 'center';
	ctx.verticalAlign = 'middle';
	ctx.font = '42px Wide Latin';
	ctx.fillText(' SPACE \n PING-PONG',400,200);
	ctx.font = '25px Wide Latin';
	ctx.fillText('Press 1 to start playing',400,300);
	 ctx.fillText('Press 2 for two players ',400,380);
	ctx.restore();
	// elije 1 jugador
	if(keydown[97] || keydown[49]){
		play = true;
		twoPlayers = false;
		snd_go.play();
		randomBackgroung();
	}
	// elije 2 jugadores
	if(keydown[98] || keydown[50]){
		play = true;
		twoPlayers = true;
		snd_go.play();
	}
}

//FUNCION PARA MOVER LOS JUGADORES, OBJETOS Y HACER LAS COLICIONES CON EL CANVAS Y LOS JUGADORES 
function movePlayers() {
		// 1 JUGADOR PARA QUE LA BOLA SIGA EL OBJETO PLAYER 2
		if(!twoPlayers && ball.dir == 1) {
		if(ball.y - 50 < player2.y) player2.y -= 5;
		if(ball.y - 50 > player2.y) player2.y += 5;
		}
	//JUGADOR 1 CON SUS COLICIONES 
	if(keydown[87]) player1.y -= 5;
	if(keydown[83]) player1.y += 5;
	if(player1.y < 0) player1.y = 0;
	if(player1.y > 500) player1.y = 500;
	 
	// JUGADOR 2  SUS COLICIONES 
	if(twoPlayers) {
	if(keydown[80]) player2.y -= 5;
	if(keydown[76]) player2.y += 5;
	}
	if(player2.y < 0) player2.y = 0;
	if(player2.y > 500) player2.y = 500;

	//VELOCIDAD DE PELOTA 
	ball.x += (4 + ball_speed_i)*ball.dir;
	ball.y += Math.sin(ball.angle)*(4 + ball_speed_i);
	if (ball.x + ball.radio > player2.x &&
		ball.y > player2.y &&
		ball.y < player2.y + player2.height)
	{
		ball.dir = -1;
		ball_speed_i += 0.40 ;
		var snd = snd_colision;
		snd.currentTime = 0;
		snd.play();
	}
	// COlicion de pelota con player 1
	if (ball.x - ball.radio < player1.x + player1.width &&
		ball.y > player1.y &&
		ball.y < player1.y + player1.height
		)
	{
		ball.dir = 1;
		ball_speed_i += 0.40;
		var snd = snd_colision;
		snd.currentTime = 0;
		snd.play();
	}
	// colision con las paredes
	if(ball.y + ball.radio > 600 && winner == 0) {
		ball.angle = -ball.angle;
		var snd = snd_colision;
		snd.currentTime = 0;
		snd.play();
	}
	if(ball.y - ball.radio < 0 && winner == 0) {
		ball.angle = -ball.angle;
		var snd = snd_colision;
		snd.currentTime = 0;
		snd.play();
	}

	// // final del juego
	if(ball.x < 10 && winner == 0) {
	 	winner = 'Player TWO';
	 	score_2 += 1;
	 	ball.x = 400;
	 }
	 if(ball.x > 800 && winner == 0) {
	 	winner = 'Player ONE';
	 	score_1 += 1;
	 	ball.x = 400;
	 }
}
  

// FUNCION PARA DIBUJAR EL MARCADOR | REANUDAR EL JUEGO | DETERMINAR UN GANADOR 
function drawText() {
	if(winner != 0) {
		ctx.save();
		ctx.shadowOffsetX = shadowOffsetY = 0;
		ctx.shadowBlur = 10;
		ctx.shadowColor = 'red';
		ctx.fillStyle = 'white';
		ctx.textAlign = 'center';
		ctx.font='30px Berlin Sans FB'
		if(score_1 < 10 && score_2 < 10 ){
		ctx.fillText('Point for  '+winner,400,300);
		ctx.fillText('press ENTER to continue',400,330);
		ball.x = 0;
		ctx.restore();
		// reanudar el juego
		if(keydown[13]) {
			winner = 0;
			ball_speed_i = 0;
			ball.angle = 120;
			ball.x = 400;
			ball.y = 300;
			ball.dir = 1;
			player1.y = player2.y = 250;
			snd_go.play();
			randomBackgroung();
		}
		}else if (score_1 ===10) {
					ctx.font="40px Berlin Sans FB";
					ctx.fillStyle='white';
					ctx.shadowColor="red";
					ctx.textAlign="Center";
					 ctx.fillText("PLAYER 1 WINS!!!", 400, 180);
					 ctx.fillText("Press F5 for main menu", 440, 220);
					 game_over.play();
			 	} else if(score_2===10) {
					 ctx.fillText("PLAYER 2 WINS!!!", 400, 180);
					 ctx.fillText("Press F5 for main menu", 400, 220);
					 game_over.play();
				
			}
			
	}}

//Mandamos llamar las clases para que se ejecute el juego 
canvas.width = 800; canvas.height = 600;
randomBackgroung();
autoScale(canvas);
function main() {
	drawBackground(); 
	if(play) {
		movePlayers();
		drawPlayers();
		drawText();
	} else { 
		getPlay();
	}
}
setInterval(main,25);
